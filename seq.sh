#!/bin/sh

IOZONE_DIR=/gpfs1/src/iozone/iozone3_487
IOZONE=${IOZONE_DIR}/src/current/iozone

DATETIME=$(date +%Y%m%d-%H%M)

# -t -F version
#IOZONE_CMD="$IOZONE -t 8 -F /gpfs3/iozone-test/writtenfile1  /gpfs3/iozone-test/writtenfile2 /gpfs3/iozone-test/writtenfile3  /gpfs3/iozone-test/writtenfile4  /gpfs3/iozone-test/writtenfile5  /gpfs3/iozone-test/writtenfile6  /gpfs3/iozone-test/writtenfile7  /gpfs3/iozone-test/writtenfile8 -s 10g -r 1m -i 0 -i 2 -l 1 -u 8  "

IOZONE_CMD="$IOZONE -f /gpfs2/iozone-test/seq.writtenfile -s 1000g -r 1m -i1 -w -+N -+E"
REPORT_OUTPUT_FILE="seq.results.${DATETIME}"

date >> $REPORT_OUTPUT_FILE
echo "Hostname that this script is running on is " $(hostname) >> $REPORT_OUTPUT_FILE
echo "Running ${IOZONE_CMD}..." >> $REPORT_OUTPUT_FILE

$IOZONE_CMD | tee -a $REPORT_OUTPUT_FILE


date >> $REPORT_OUTPUT_FILE

